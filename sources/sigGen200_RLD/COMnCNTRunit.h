/*
 * COMnCNTRunit.h
 *
 *  Created on: Jun 9, 2017
 *      Author: adrian
 */

#ifndef COMNCNTRUNIT_H_
#define COMNCNTRUNIT_H_

#include "globalPara.h"


class COMnCNTRunit
/*
 * This class describes the Command and Control Unit of the sigGen200 system
 * It has methods for getting the 5 buttons states from the front panel of the device
 * It has a built in mechanism to get only the transition from not-pressed to pressed for a button
 * This built in mechanism prevents multiple increments of a parameter when the user only pressed once a button
 */
{
private:
	uint8_t upsw_laststate;
	uint8_t downsw_laststate;
	uint8_t lsw_laststate;
	uint8_t rsw_laststate;
	uint8_t wvsw_laststate;
public:
	//I18N
	void I18N(void);
	uint8_t checkState(void);

};


#endif /* COMNCNTRUNIT_H_ */
