/*
 * COMnCNTRunit.cpp
 *
 *  Created on: Jun 9, 2017
 *      Author: adrian
 */


#include "globalPara.h"
#include "COMnCNTRunit.h"


void COMnCNTRunit::I18N(void)
/*
 *	Name: I18N (Initialization)
 *	Parameters:	void
 *	Return:	void
 *	Description:
 *		This method sets up the Atmega Pins for Comand and Control Unit.
 *		It does the setting using the global defines in globalPara.h under the tag "defines for COMM&CONTROL unit"
 *		It also makes initializations for laststate private members of class to default 1
 */
{
	//sw buttons as input (DDRn 0)
	DDRD&=~(1<<WVSW);
	DDRB&=~(1<<UPSW|1<<DOWNSW|1<<LSW|1<<RSW);
	//sw buttons with pull up resistors (PORTxn 1)
	PORTD|=(1<<WVSW);
	PORTB|=(1<<UPSW|1<<DOWNSW|1<<LSW|1<<RSW);

	this->upsw_laststate=1;
	this->downsw_laststate=1;
	this->lsw_laststate=1;
	this->rsw_laststate=1;
	this->wvsw_laststate=1;

}

uint8_t COMnCNTRunit::checkState(void)
/*
 *	Name: checkState
 *	Parameters:	void
 *	Return:
 *		uint8_t activated button code (only once per call of function) and 0 if none is active (pressed)
*		active button is coded according to defines in globalPara.h under the tag "small codification for switches keys @the system"
 *	Description:
 *		this method only returns the code of an active button at a time of 0 if none is pressed
 *		the method has an smart way of detecting the transition from 1 (not pressed) to 0 (pressed) by always saving the last state in
 *		class member <sw_name>_laststate variable
 *
 */
{
	if((PINB&(1<<UPSW))==0x00) 			//button pressed
	{
		if(this->upsw_laststate==1)		//if last state was 'NOT PRESSED'
		{
			this->upsw_laststate=0;
			return PUSHUP;
		}
	}
	else
		this->upsw_laststate=1;
	if((PINB&(1<<DOWNSW))==0x00)
	{
		if(this->downsw_laststate==1)
		{
			this->downsw_laststate=0;
			return PUSHDOWN;
		}
	}
	else
		this->downsw_laststate=1;
	if((PINB&(1<<LSW))==0x00)
	{
		if (this->lsw_laststate==1)
		{
			this->lsw_laststate=0;
			return PUSHLEFT;
		}
	}
	else
		this->lsw_laststate=1;
	if((PINB&(1<<RSW))==0x00)
	{
		if (this->rsw_laststate==1)
		{
			this->rsw_laststate=0;
			return PUSHRIGHT;
		}
	}
	else
		this->rsw_laststate=1;
	if((PIND&(1<<WVSW))==0x00)
	{
		if(this->wvsw_laststate==1)
		{
			this->wvsw_laststate=0;
			return PUSHWAVE;
		}
	}
	else
		this->wvsw_laststate=1;
	return 0;
}


