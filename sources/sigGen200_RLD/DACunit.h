/*
 * DACunit.h
 *
 *  Created on: Jun 9, 2017
 *      Author: adrian
 */

#ifndef DACUNIT_H_
#define DACUNIT_H_

#include "globalPara.h"
#include "LCDDisplay.h"
class LCDDisplay;

//	declare TIMER1_COMPA_vect Interrupt service routine as a external method for this class
//	this will allow the IRS mentioned above to access the methods of this class and change some parameters
extern "C" void TIMER1_COMPA_vect(void) __attribute__ ((signal));

class DACunit
/*
 *	This class describes the DAC (Digital to Analog Convertor)
 *	It has classic methods for getting and setting private members values
 *	It has a I18N (initialization) method for the DAC hardware component
 *	It also has increment and decrement methods for private parameters. These methods will be accessed when the user presses the UP/DOWN button
 */
{

private:
	uint8_t signal_type;		//the current signal type selected in the system in the codification listed in globalPara
								//under the tag "small codification for signal types @the system"
	uint8_t cnt;				//this counter in incremented periodically and is used to index the vector of values for some signals
								//for other signals the vector of values is not needed since they have a simple mathematical formula (such as square)
	int8_t  dir; 				//this is only for triangle signal, this variable is the direction
	uint16_t freq;				//this is the frequency in Hz (if kHz=0) or in kHz (if kHz=1)
	uint8_t khz;				//above mentioned
	uint8_t duty;				//only for square signal is the duty cycle
public:
	//friend classes and function which can access all members (private and public) of this class
	friend void TIMER1_COMPA_vect();
	friend class LCDDisplay;

	//I18N (Initialization)
	void I18N(void);

	//getters
	uint8_t getSignalType(void);
	uint16_t getFreq(void);
	uint8_t getDuty(void);

	//setters
	void setTimerRegister(uint16_t new_value);
	void setSignal(uint8_t new_signal);

	//increments
	void incrementCnt(void);
	void incrementFreq(uint8_t add);
	void incrementDuty(uint8_t add);
	void incrementParameter(LCDDisplay MyLCD);

	//decrements
	void decrementFreq(uint8_t dec);
	void decrementDuty(uint8_t dec);
	void decrementParameter(LCDDisplay MyLCD);

};

#endif /* DACUNIT_H_ */
