/*
 * main.c
 *
 *  Created on: Mar 11, 2017
 *      Author: Adrian-Razvan Petre (razdrian)
 *      GitHub repo: www.github.com/razdrian/sigGen200
 *
 */

#include "globalPara.h"
#include "signals.h"


#include "DACunit.h"
#include "LCDDisplay.h"
#include "COMnCNTRunit.h"

DACunit MyDAC;					//MyDAC object of type DACunit
LCDDisplay MyLCD;				//MyLCD object of type LCDDisplay
COMnCNTRunit MyCNTunit;			//MyCNTunit object of type COMnCNTRunit

int main()
{
	MyDAC.I18N();
	MyLCD.I18N();
	MyCNTunit.I18N();
	_delay_ms(10);

	MyLCD.enableTIMER2();

	//evaluate the COMMnCNTunit input here in main, in the main loop
	while(1)
	{
		switch(MyCNTunit.checkState())			//switch according to the button pressed in the MyCNTunit
		{
		case PUSHUP:
			MyLCD.resetBlinkCounter();

			MyDAC.incrementParameter(MyLCD);

			MyLCD.setBlinkStage();
			break;
		case PUSHDOWN:
			MyLCD.resetBlinkCounter();
			MyDAC.decrementParameter(MyLCD);
			MyLCD.setBlinkStage();
			break;
		case PUSHLEFT:
			MyLCD.resetBlinkCounter();
			MyLCD.incrementBlinkState(MyDAC);
			break;
		case PUSHRIGHT:
			MyLCD.resetBlinkCounter();
			MyLCD.decrementBlinkState(MyDAC);
			break;
		case PUSHWAVE:
			MyDAC.setSignal((MyDAC.getSignalType()+1)%5);
			MyLCD.disableTIMER3();
			MyLCD.updateMissingChar(MyDAC);		// update missing char to prevent a char missing from the display when
			MyLCD.resetBlinkState();
			MyLCD.enableTIMER2();
		}
		// the delay between two successive checks of MyCNTunit should be in real tile aprox. 100ms BUT
		// this main infinite loop is interrupted of 3 ISR from TIMER1 TIMER2 and TIMER3 and this makes
		// the main infinite loop to repeat with a higher period
		_delay_us(100);
	}
	return 0;

}

/*
 * Interrupt Service Routines (ISRs)
 */
ISR(TIMER3_COMPA_vect)
/*
 * this ISR controls the blinking of the DUB (Digit Under Blink)
 * this ISR has very low priority (the lowest of all the ISRs)
 */
{
	if(MyLCD.getBlinkCounter()>MAXBLINKS)	//if Blink Counter has reached a maximum limit update Missing Char and reset Blink State (turn of Blinking)
	{
		MyLCD.disableTIMER3();
		MyLCD.updateMissingChar(MyDAC);
		MyLCD.resetBlinkState();
	}
	else
		MyLCD.incrementBlinkCounter();		//else increment Blink Counter and let him grow

	switch(MyLCD.blink_state)
	{
	case FREQU_CODE:
		if(MyLCD.blink_stage)		//if it's in stage 1 write to display the corresponding char
			//add to the integer value of the digit the character '0' to transform it into a char
			MyLCD.sendCharacter(MyDAC.getFreq()%10+'0',1,FREQU_ADD);
		else						//else if it's in stage 0 display to LCD a blank space
			MyLCD.sendCharacter(' ',1,FREQU_ADD);
		break;
	case FREQT_CODE:
		if(MyLCD.blink_stage)
			MyLCD.sendCharacter(MyDAC.getFreq()/10%10+'0',1,FREQT_ADD);
		else
			MyLCD.sendCharacter(' ',1,FREQT_ADD);
		break;
	case FREQH_CODE:
		if(MyLCD.blink_stage)
			MyLCD.sendCharacter(MyDAC.getFreq()/100+'0',1,FREQH_ADD);
		else
			MyLCD.sendCharacter(' ',1,FREQH_ADD);
		break;
	case DUTYU_CODE:
		if(MyLCD.blink_stage)
			MyLCD.sendCharacter(MyDAC.getDuty()%10+'0',1,DUTYU_ADD);
		else
			MyLCD.sendCharacter(' ',1,DUTYU_ADD);
		break;
	case DUTYT_CODE:
		if(MyLCD.blink_stage)
			MyLCD.sendCharacter(MyDAC.getDuty()/10+'0',1,DUTYT_ADD);
		else
			MyLCD.sendCharacter(' ',1,DUTYT_ADD);
		break;
	case IDLE_CODE:
		break;
	}
	MyLCD.blink_stage^=1;			//set to 0 if it was 1 and to 1 if it was 0 the blink_stage
}

ISR(TIMER2_COMP_vect)
/*
 * this ISR refreshes the display according to the users input in MyCntUnit
 * has a medium level of priority, higher than the one updating the blinking digit and lower than the one updating DAC
 */
{
	switch(MyDAC.getSignalType())
	{
	case SINE:
		MyLCD.clearLine(0);
		MyLCD.refreshWord(0x81,"SINE");				//the hex number is the data in which is packed the starting address according to the Datasheet
		MyLCD.clearLine(1);
		MyLCD.refreshWord(0xC0,"Frequency:");
		MyLCD.sendCharacter(MyDAC.getFreq()%10+'0',1,FREQU_ADD);	//for sendCharacter is the same rule: The data is needed
																	//in which the address should be packed following the same protocol
																	//see the glopabPara.h define under the tag "addresses for DUB"
		MyLCD.sendCharacter(MyDAC.getFreq()/10%10+'0',1,FREQT_ADD);
		MyLCD.sendCharacter(MyDAC.getFreq()/100+'0',1,FREQH_ADD);
		MyLCD.refreshWord(FREQU_ADD+2,"Hz");
		break;
	case SQUARE:
		MyLCD.clearLine(0);
		MyLCD.refreshWord(0x81,"SQUARE Duty:");
		MyLCD.sendCharacter(MyDAC.getDuty()%10+'0',1,DUTYU_ADD);
		MyLCD.sendCharacter(MyDAC.getDuty()/10+'0',1,DUTYT_ADD);
		MyLCD.refreshWord(DUTYU_ADD+1,"%");
		break;
	case SAWTOOTH:
		MyLCD.clearLine(0);
		MyLCD.refreshWord(0x81,"SAWTOOTH");
		break;
	case TRIANGLE:
		MyLCD.clearLine(0);
		MyLCD.refreshWord(0x81,"TRIANGLE");
		break;
	case NOISE:
		MyLCD.clearLine(0);
		MyLCD.refreshWord(0x81,"NOISE");
		MyLCD.clearLine(1);
		MyLCD.refreshWord(0xC0,"Frequency:Undef.");
		break;
	default:
		break;
	}
	MyLCD.disableTIMER2();
}

ISR(TIMER1_COMPA_vect)
/*
 * this ISR outputs the data to the DAC and it has to be very fast!
 * this ISR has very high priority
 * the number output for the DAC unit is computed according to the sinesig[] vector for sine signal
 * and according to some simple mathematical formulas for the ohter signals
 */
{
	switch(MyDAC.signal_type)
	{
	case SINE:
		PORTD&=~(1<<WR);			//Data sent to 8bits input is taken into consideration of the DAC only if WR bit is active (set to 0)
		asm volatile ("nop");		//the smallest quanta of time available in the program, the smallest delay corresponding to a NOP operation
		PORTC =sinesig[MyDAC.cnt];	//send to port the corresponding number
		asm volatile ("nop");		//another very small display
		PORTD|=(1<<WR);				//disable active port WR now
		break;
	case SAWTOOTH:
	case TRIANGLE:
	case NOISE:
		PORTD&=~(1<<WR);
		asm volatile ("nop");
		PORTC =MyDAC.cnt;
		asm volatile ("nop");
		PORTD|=(1<<WR);
		break;
	case SQUARE:
		PORTD&=~(1<<WR);
		asm volatile ("nop");
		if(MyDAC.cnt<128)
			PORTC =0x00;
		else
			PORTC =0xFF;
		asm volatile ("nop");
		PORTD|=(1<<WR);
		break;
	default:
		break;
	}
	MyDAC.incrementCnt();			//increment cnt using the MyDAC method which will take care of all the needed limitations
}
