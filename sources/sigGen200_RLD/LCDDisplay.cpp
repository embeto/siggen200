/*
 * LCDDisplay.cpp
 *
 *  Created on: Jun 9, 2017
 *      Author: adrian
 */

#include "globalPara.h"
#include "LCDDisplay.h"

#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <stdlib.h>


//I18N (initialization)
void LCDDisplay::I18N(void)
/*
 *	Name: I18N (Initialization)
 *	Parameters:	void
 *	Return: void
 *	Description:
 *		I18N (Initialization) method for the LCDDisplay
 *		Has 3 main stages:
 *			+ sets the private members of the class to a default state
 *			+ sets the LCD mode of work to the preferred one
 *			+ sets the two timers (enable timer/interrupt enable/compare with OCR register mode)
 */
{
	//class private member initialization
	this->blink_state=IDLE_CODE;
	this->blink_stage=1;
	this->blink_counts=0;

	//First of all the display port initialization
	DDRA=0xFF;
	DDRD|=(1<<RS|1<<RW|1<<E);

	//classic display initialization according to datasheet available here
	// http://www.tme.eu/ro/Document/4569345c680be141ffa82ca5c2d9f6b4/RC1602B2-BIW-CSX.pdf
	_delay_ms(40);					//delay after port initialization
	sendCommand(0b00111100);		// Set interface data length 0.0.1.DL_N.F.X.X
	_delay_us(40);
	sendCommand(0b00001100);		// Display On/Off control 0.0.0.0_1.D.C.B
	_delay_us(40);
	sendCommand(0b00000001); 		// Clear Display 0x01 = 00000001
	_delay_ms(4);

	//Blinking of the DUB is on TIMER3, which has the lowest priority among the timers ISRs
	//the lowest priority is used here because blinking the DUB one millisecond later is not important
	sei();
	TCCR3B|=1<<CS32|1<<CS30|1<<WGM32;			//fCLK/1024-->fTIM=16khz --> TTIM=62us
	OCR3A=0x2180;								//OCR3A=1600DEC -> fISR=1s
	ETIMSK|=(1<<OCIE3A);

	//Refreshing waveform information on the LCD is made by TIMER2
	TCCR2|=1<<CS22|1<<CS21|1<<WGM21;			//fCLK/256-->fTIM=64khz --> TTIM=15us
	OCR2=0x80;									//OCR3A=1600DEC -> fISR=1s
}

//setters
void LCDDisplay::setBlinkState(uint8_t blink_state)
/*
 *	Name: setBlinkState
 *	Parameters:	uint8_t blink_state new blink state according to the codification in globalPara.h
 *	Return:
 *		void
 *	Description:
 *		Sets a new value for the blink_state of the private member of the class
 */
{
	this->blink_state=blink_state;
}

void LCDDisplay::setBlinkStage(void)
/*
 *	Name: setBlinkStage
 *	Parameters:	void
 *	Return:
 *		void
 *	Description:
 *		Sets the blink_stage to 1
 */
{
	this->blink_stage=1;
}

void LCDDisplay::enableTIMER2(void)
/*
 *	Name: enableTIMER2
 *	Parameters:	void
 *	Return:
 *		void
 *	Description:
 *		Sets the OCIE2 active, Output Compare Interrupt Enable Register 2
 */
{
	TIMSK|=(1<<OCIE2);						//output compare interrupt enable
}

void LCDDisplay::enableTIMER3(void)
/*
 *	Name: enableTIMER3
 *	Parameters:	void
 *	Return:
 *		void
 *	Description:
 *		Sets the OCIE3A active, Output Compare Interrupt Enable Register 3A
 */
{
	ETIMSK|=(1<<OCIE3A);					//output compare interrupt enable
}

//getters
uint8_t LCDDisplay::getBlinkCounter(void)
/*
 *	Name: getBlinkCounter
 *	Parameters:	void
 *	Return:
 *		uint8_t blink_counts
 *	Description:
 *		returns the blink_counts
 */
{
	return this->blink_counts;
}


//reseters
void LCDDisplay::resetBlinkCounter(void)
/*
 *	Name: resetBlinkCounter
 *	Parameters:	void
 *	Return:
 *		void
 *	Description:
 *		resets the blink_counts to 0
 */
{
	this->blink_counts=0;
}

void LCDDisplay::resetBlinkState(void)
/*
 *	Name: resetBlinkState
 *	Parameters:	void
 *	Return:
 *		void
 *	Description:
 *		resets the blink_state to IDLE_STATE (no digit blinking)
 */
{
	this->blink_state=IDLE_CODE;
}

void LCDDisplay::resetBlinkStage(void)
/*
 *	Name: resetBlinkStage
 *	Parameters:	void
 *	Return:
 *		void
 *	Description:
 *		resets the blink_stage to 0
 */
{
	this->blink_stage=0;
}

void LCDDisplay::disableTIMER2(void)
/*
 *	Name: disableTIMER2
 *	Parameters:	void
 *	Return:
 *		void
 *	Description:
 *		disable Timer2 by setting to 0 the OCIE1A bit
 */
{
	TIMSK&=~(1<<OCIE2);						//output compare interrupt disable
}


void LCDDisplay::disableTIMER3(void)
/*
 *	Name: disableTIMER3
 *	Parameters:	void
 *	Return:
 *		void
 *	Description:
 *		disable Timer3 by setting to 0 the OCIE3A bit
 */
{
	ETIMSK&=~(1<<OCIE3A);						//output compare interrupt disable
}


void LCDDisplay::incrementBlinkCounter(void)
/*
 *	Name: incrementBlinkCounter
 *	Parameters:	void
 *	Return:
 *		void
 *	Description:
 *		increment Blink Counter
 *		blink_counter has the number of blinks since the begin of a blink stage
 */
{
	this->blink_counts++;
}

void LCDDisplay::incrementBlinkState(DACunit MyDAC)
/*
 *	Name: incrementBlinkState
 *	Parameters:	DACunit MyDAC
 *				this class (LCDDisplay is friend class of DACunit)
 *	Return:
 *		void
 *	Description:
 *		increment Blink State (e.g. from FREQU_CODE to FREQT_CODE and after FREQH_CODE)
 *		blink_counter has the number of blinks since the begin of a blink stage
 */
{
	this->disableTIMER3();				//disable the TIMER3 while changing the blink stage
	if(~this->blink_stage)
		this->updateMissingChar(MyDAC);	//prevent a char to remain not printed on the display before moving to another one
	this->resetBlinkStage();			//reset blink stage before starting a new stage

	switch(MyDAC.getSignalType())		//according to signal Type change the blink state because for some signals more parameters are available
										//for example for square has also duty units and tens where the cursor should be present after some clicks
	{
	case SAWTOOTH:
	case TRIANGLE:
	case SINE:
		if(this->blink_state==IDLE_CODE)
			this->blink_state=FREQU_CODE;
		else
			this->blink_state=((this->blink_state+1)%3)+0xF0;
		break;
	case SQUARE:
		if(this->blink_state==IDLE_CODE)
			this->blink_state=FREQU_CODE;
		else
			this->blink_state=((this->blink_state+1)%5)+0xF0;
		break;
	case NOISE:
		this->resetBlinkState();
		break;
	default:
		break;
	}
	this->enableTIMER3();				//enable back the TIMER3 after changes have been made
}

void LCDDisplay::decrementBlinkState(DACunit MyDAC)
/*
 *	Name: decrementBlinkState
 *	Parameters:	DACunit MyDAC
 *				this class (LCDDisplay is friend class of DACunit)
 *	Return:
 *		void
 *	Description:
 *		decrement Blink State (e.g. from FREQH_CODE to FREQT_CODE and after FREQU_CODE)
 *		blink_counter has the number of blinks since the begin of a blink stage
 */
{
	//same observations as incrementBlinkState
	this->disableTIMER3();
	if(~this->blink_stage)
		this->updateMissingChar(MyDAC);
	this->resetBlinkStage();
	switch(MyDAC.getSignalType())
	{
	case SAWTOOTH:
	case TRIANGLE:
	case SINE:
		if(this->blink_state==IDLE_CODE)
			this->blink_state=FREQH_CODE;
		else
			if(this->blink_state>0xF0)
				this->blink_state--;
			else
				this->blink_state=0xF2;
		break;
	case SQUARE:
		if(this->blink_state==IDLE_CODE)
			this->blink_state=DUTYT_CODE;
		else
			if(this->blink_state>0xF0)
				this->blink_state--;
			else
				this->blink_state=0xF4;
		break;
	case NOISE:
		this->resetBlinkState();
		break;
	default:
		break;
	}
	this->enableTIMER3();
}

void LCDDisplay::updateMissingChar(DACunit MyDAC)
/*
 *	Name: updateMissingChar
 *	Parameters:	DACunit MyDAC
 *				this class (LCDDisplay is friend class of DACunit)
 *	Return:
 *		void
 *	Description:
 *		according to the MyDAC frequency and duty cycle variable sends to LCD display a new character to display
 *		the new character to be updated is determined according to the blink_state
 */
{
	switch(this->blink_state)	//with % and / operators the units, tens or hundreds can be accessed from a number
	{
	case FREQU_CODE:
		this->sendCharacter(MyDAC.getFreq()%10+'0',1,FREQU_ADD);
		break;
	case FREQT_CODE:
		this->sendCharacter(MyDAC.getFreq()/10%10+'0',1,FREQT_ADD);
		break;
	case FREQH_CODE:
		this->sendCharacter(MyDAC.getFreq()/100+'0',1,FREQH_ADD);
		break;
	case DUTYU_CODE:
		this->sendCharacter(MyDAC.getDuty()%10+'0',1,DUTYU_ADD);
		break;
	case DUTYT_CODE:
		this->sendCharacter(MyDAC.getDuty()/10+'0',1,DUTYT_ADD);
		break;
	case IDLE_CODE:
		break;
	}
}

void LCDDisplay::latchEnable()
/*
 *	Name: latchEnable
 *	Parameters:	void
 *	Return:
 *		void
 *	Description:
 *		enable latch for display, this is from the internal protocol of the LCD
 */
{
	LCDCNTPORT |= (1<<E); 		//Turn Enable on
	//TODO change to a "nop" in the end, when the CPU is loaded with lots of ISRs
	_delay_us(1);
	LCDCNTPORT &= ~(1<<E); 		//turn off Enable to let LCD run smooth
}

void LCDDisplay::CheckIfBusy()
/*
 *	Name: CheckIfBusy
 *	Parameters:	void
 *	Return:
 *		void
 *	Description:
 *		if the LCD is busy wait for it to finish what is does
 *		the call of this function is blocking, it waits until LCD is no longer busy
 */
{
	DDRA = 0x00; 				//Put LCDPORT in Input (read) Mode
	LCDCNTPORT &= ~(1<<RS); 	//Turn on LCD Command Mode (RS off)
	LCDCNTPORT |= (1<<RW); 		//Set LCD to Read (RW on)
	while (LCDPORT >= 0x80) 	//LCD_DATA7 pin will be a "1" (busy)
	{
		this->latchEnable(); 			//routine to turn the enable (E) on and off
	}
	DDRA = 0xFF; 				//Set LCDPORT as output
}

void LCDDisplay::sendCommand(unsigned char command)
/*
 *	Name: sendCommand
 *	Parameters: unsigned char command
 *	Return:
 *		void
 *	Description:
 *		sends to the LCD a command (it is a 8 bits command)
 */
{
	this->CheckIfBusy();
	DDRA=0xFF;
	LCDPORT= command;
	LCDCNTPORT &= ~((1<<RS)|(1<<RW)); 	//turn off RS (command mode) and RW (write mode)
	this->latchEnable();
	DDRA = 0x00;
}

void LCDDisplay::sendCharacter(unsigned char character, uint8_t is_newln, uint8_t newln)
/*
 *	Name: sendCharacter
 *	Parameters: unsigned char character,
 *				uint8_t is_newln, 			//ad a new line after character
 *				uint8_t newln				//new line code
 *	Return:
 *		void
 *	Description:
 *		sends to the LCD a character and a new line after it if the is_newln is set
 */
{
	if (is_newln==1)
	{
		this->sendCommand(newln);
		_delay_us(40);
	}
	else
		this->CheckIfBusy();

	DDRA=0xFF;
	LCDPORT = character;
	LCDCNTPORT &= ~(1<<RW); 	//turn off RW (write mode)
	LCDCNTPORT |= (1<<RS); 		//turn on RS (character display mode)
	this->latchEnable();
	DDRA = 0x00;
}

void LCDDisplay::refresh(char line1[16],char line2[16])
/*
 *	Name: refresh
 *	Parameters: char line1[16],		//first line of display
 *				char line2[16]		//second line of display
 *	Return:
 *		void
 *	Description:
 *		refresh the whole display and for the first line has line1 array and for the second line has line2
 */
{

	uint8_t i;
	this->clearLine(2);
	this->sendCharacter(line1[0],1,0x80);
	for(i=1;i<strlen(line1);i++)
		this->sendCharacter(line1[i],0,0xFF);

	this->sendCharacter(line2[0],1,0xC0);
	for(i=1;i<strlen(line2);i++)
		this->sendCharacter(line2[i],0,0xFF);

}

void LCDDisplay::refreshWord(uint8_t starting_addr, char* word)
/*
 *	Name: refresh
 *	Parameters: char line1[16],		//first line of display
 *				char line2[16]		//second line of display
 *	Return:
 *		void
 *	Description:
 *		refresh the whole display and for the first line has line1 array and for the second line has line2
 */
{
	uint8_t i;
	if(strlen(word)>0)
	{
		this->sendCharacter(word[0],1,starting_addr);
		for(i=1;i<strlen(word);i++)
			this->sendCharacter(word[i],0,0xFF);
	}
}

void LCDDisplay::clearLine(uint8_t line)
/*
 *	Name: refresh
 *	Parameters: uint8_t line,		//first line of display
 *	Return:
 *		void
 *	Description:
 *		clear the content of a whole line in display according to the line parameter, if line=2 clear the whole display
 */
{
	uint8_t i;
	switch(line)
	{
	case 0:
		this->sendCharacter(' ',1,0x80);
		for(i=1;i<16;i++)
			this->sendCharacter(' ',0,0xFF);
		break;
	case 1:
		this->sendCharacter(' ',1,0xC0);
		for(i=1;i<16;i++)
			this->sendCharacter(' ',0,0xFF);
		break;
	case 2:
		this->sendCommand(0b00000001); 		// Clear Display 0x01 = 00000001
		_delay_ms(2);
		break;
	}
}
