/*
 * LCDDisplay.h
 *
 *  Created on: Jun 9, 2017
 *      Author: adrian
 */

#ifndef LCDDISPLAY_H_
#define LCDDISPLAY_H_

#include "globalPara.h"
#include "DACunit.h"

//prototype of DACunit class to be recognized when declared as friend class in DACunit
class DACunit;

//	declare TIMER2_COMP_vect and TIMER3_COMPA_vect Interrupt service routine as a external method for this class
//	this will allow the both IRSs mentioned above to access the methods of this class and change some parameters
extern "C" void TIMER2_COMP_vect(void) __attribute__ ((signal));
extern "C" void TIMER3_COMPA_vect(void) __attribute__ ((signal));


class LCDDisplay
/*
 * This class describes the LCDDisplay of the sigGen200 system link below
 * 		http://www.tme.eu/ro/details/rc1602b2-biw-csx/afisaje-lcd-alfanumerice/raystar-optronics/ (see also datasheet here)
 * It has classic setters and getters methods for private members of the class
 * It has reset methods for private members of the class
 * It has increment and decrement methods for private members of the class
 * It has LCD specific methods for sending new char to device (LCD)
 */
{
private:
	//private variables for blink state when user is setting some parameters

	//State of the current blink state coded according to codes in globalPara.h under the tag
	//	"small codification for DUB (digit under blink) @the system"
	//more about the DUB codification in the globalPara.h
	uint8_t blink_state;

	//number of blinks completed since the begin of a new stage
	//used to stop the blinking of a DUB after a certain number of blink
	uint8_t blink_counts;

	//the current stage of blink. Can only be 1 (digit on) of 0 (digit off)
	bool blink_stage;

public:
	//friend methods and classes
	friend void TIMER2_COMP_vect();
	friend void TIMER3_COMPA_vect();
	friend class DACunit;

	// I18N (initialization)
	void I18N(void);

	//setters
	void setBlinkState(uint8_t blink_state);
	void setBlinkStage(void);
	void enableTIMER2(void);
	void enableTIMER3(void);

	//getters
	uint8_t getBlinkCounter(void);

	//reset methods
	void resetBlinkState(void);
	void resetBlinkStage(void);
	void resetBlinkCounter(void);
	void disableTIMER2(void);
	void disableTIMER3(void);

	//increment methods
	void incrementBlinkState(DACunit MyDAC);
	void incrementBlinkCounter(void);

	//decrement methods
	void decrementBlinkState(DACunit MyDAC);
	void updateMissingChar(DACunit MyDAC);

	//LCD specific methods
	void latchEnable();
	void CheckIfBusy();
	void sendCommand(unsigned char command);
	void sendCharacter(unsigned char character, uint8_t is_newln, uint8_t newln);

	void refresh(char line1[16],char line2[16]);
	void refreshWord(uint8_t starting_addr, char* word);
	void clearLine(uint8_t line);

};

#endif /* LCDDISPLAY_H_ */
