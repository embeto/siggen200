/*
 * globalPara.h
 *
 *  Created on: Jun 9, 2017
 *      Author: adrian
 */

#ifndef GLOBALPARA_H_
#define GLOBALPARA_H_

// defines for DAC unit
#define DACPORT PORTC
#define DACPIN PINC
#define WR PORTD3

// defines for LCD unit
#define LCDPORT PORTA
#define LCDPIN PINA
#define LCDCNTPORT PORTD
#define LCDCNTPIN PIND
#define RS PORTD2
#define RW PORTD4
#define E PORTD5

// defines for COMM&CONTROL unit
#define UPSW PORTB1
#define DOWNSW PORTB2
#define LSW PORTB4
#define RSW PORTB3
#define WVSW PORTD6

//create a small codification for signal types @the system
# define SINE 		0x00
# define SQUARE		0x01
# define SAWTOOTH	0x02
# define TRIANGLE	0x03
# define NOISE		0x04

//create a small codification for switches keys @the system
# define PUSHUP 	0xE0
# define PUSHDOWN	0xE1
# define PUSHLEFT	0xE2
# define PUSHRIGHT	0xE3
# define PUSHWAVE	0xE4

//create a small codification for DUB (digit under blink) @the system
#define FREQU_CODE	0xF0	//DUB is Units of Frequency
#define FREQT_CODE	0xF1	//DUB is Tens of Frequency
#define FREQH_CODE	0xF2	//DUB is Hundreds of Frequency
#define DUTYU_CODE	0xF3	//DUB is Units of Duty
#define DUTYT_CODE	0xF4	//DUB is Tens of Duty
#define IDLE_CODE	0xFF	//DUB is Idle (no digit blinking)

//addresses for DUB (digits under blink)
#define FREQU_ADD	0xCC
#define FREQT_ADD	0xCB
#define FREQH_ADD	0xCA
#define DUTYU_ADD	0x8E
#define DUTYT_ADD	0x8D


#define MAXBLINKS	8
//this define is mandatory to be placed before including delay.h
#define F_CPU 16000000UL


#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <stdlib.h>



#endif /* GLOBALPARA_H_ */
