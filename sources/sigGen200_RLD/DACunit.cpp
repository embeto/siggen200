/*
* DACunit.cpp
*
*  Created on: Jun 9, 2017
*      Author: adrian
*/

#include "globalPara.h"
#include "DACunit.h"

//this vector contains default values for OCR1A register of Timer1
//the OCR1A is compared with the current number in the TImer1A register and when matches, the Timer executes the ISR and restarts
//by changing the OCR1A value, the frequency of the ISR execution will change
//in the current implementation, only 10 values for the frequency are allowed
//the one at position 1 corresponds to 100Hz and so on ( freqTAB[i]<--->freq(HZ)/100
//this values were determined empirically
uint16_t freqTAB[]={0x0FFF,0x0774,0x03BA,0x027C,0x01DD,0x017D,0x0120,0x010F,0x00E9,0x0055};

//I18N (Initialization)
void DACunit::I18N(void)
/*
 *	Name: I18N (Initialization)
 *	Parameters:	void
 *	Return: void
 *	Description:
 *		I18N (Initialization) method for the DACunit
 *		Sets initial default values for the DAC parameters
 */
{
	//DAC BUS pins and WR
	DDRC|=0xFF;
	DDRD|=(1<<WR);

	this->freq=100;
	this->duty=50;
	this->signal_type=SINE;
	this->cnt=0;
	this->dir=1;
	this->khz=0;

	//DACunit is on TIMER3
	//configuration of the timer (enable/Wave Generation Mode/etc)
	//sei() -> Set Global Interrupt Flag
	sei();
	TCCR1B|=1<<CS10|1<<WGM12;			//fCLK-->fTIM=16Mhz --> TTIM=62.5ns

	//TODO implement a dynamic change of this settings to keep up with the users demands
	OCR1A=freqTAB[1];					//set a default value for OCR1A (Output Compare Register 1A of Timer 1)

	TIMSK|=(1<<OCIE1A);					//output compare interrupt enable, enable Timer 1 to execute ISR when matches OCR1A
}

//getters
uint8_t DACunit::getSignalType(void)
/*
 *	Name: getSignalType
 *	Parameters:	void
 *	Return:
 *		uint8_t current signal_type of the DACunit in the codification listed in globalPara.h under the tag "small codification for signal"
 *	Description:
 *		Simple getter for signal_type private member of the class
 */
{
	return this->signal_type;
}

uint16_t DACunit::getFreq(void)
/*
 *	Name: getFreq
 *	Parameters:	void
 *	Return:
 *		uint8_t current Frequency for the system
 *	Description:
 *		Simple getter for freq private member of the class
 */
{
	return this->freq;
}

uint8_t DACunit::getDuty(void)
/*
 *	Name: getDuty
 *	Parameters:	void
 *	Return:
 *		uint8_t current Duty Cycle for the system
 *	Description:
 *		Simple getter for duty private member of the class
 */
{
	return this->duty;
}

//setters
void DACunit::setTimerRegister(uint16_t new_value)
/*
 *	Name: setTimerRegister
 *	Parameters:	uint16_t new_value
 *	Return:
 *		void
 *	Description:
 *		Sets a new value for the OCR1A register
 */
{
	OCR1A=new_value;
}

void DACunit::setSignal(uint8_t new_signal)
/*
 *	Name: setSignal
 *	Parameters:	uint8_t new_signal type in codification mentioned in globalPara.h
 *	Return:
 *		void
 *	Description:
 *		After a new signal type is set, some additional parameter of the class must be set
 *		cnt set to 0 to start from the fist sample of the new signal type
 *		dir set to 1 to go up for the first time for this signal
 *
 */
{
	this->signal_type=new_signal;
	this->cnt=0;
	this->dir=1;
}

//increments
void DACunit::incrementCnt(void)
/*
 *	Name: incrementCnt
 *	Parameters:	void
 *	Return:
 *		void
 *	Description:
 *		This methods is an important one for the DAC unit
 *		according to the signal_type it increments the cnt and also resets the cnt to 0 when needed
 *
 */
{
	switch(this->signal_type)
	{
	case SINE:
		this->cnt=(this->cnt+1)%256;
		break;
	case SQUARE:
		this->cnt=(this->cnt+1)%256;
		break;
	case SAWTOOTH:
		this->cnt=(this->cnt+1)%256;
		break;
	case TRIANGLE:
		this->cnt+=this->dir;
		if(this->cnt==255)
			this->dir=-1;
		if(this->cnt==0)
			this->dir=1;
		break;
	case NOISE:
		this->cnt=rand()%256;
		break;
	default:
		break;
	}
}

void DACunit::incrementFreq(uint8_t add)
/*
 *	Name: incrementFreq
 *	Parameters:	uint8_t add addition to frequency parameter
 *	Return:
 *		void
 *	Description:
 *		This methods increments the Frequency parameter with some limitations
 */
{
	if((this->freq+add)<=999)
		this->freq+=add;
	//TODO in a future enhancement this method should also update the khz variable if this->freq+add is larger than 999
}

void DACunit::incrementDuty(uint8_t add)
/*
 *	Name: incrementDuty
 *	Parameters:	uint8_t add addition to duty parameter
 *	Return:
 *		void
 *	Description:
 *		This methods increments the duty parameter with some limitations
 */
{
	if((this->duty+add)<=99)
		this->duty+=add;
}

void DACunit::incrementParameter(LCDDisplay MyLCD)
/*
 *	Name: incrementParameter
 *	Parameters:
 *		LCDDisplay MyLCD object
 *		The DACunit class is friend of LCDDisplay and DACunit will be able to access all private and public members
 *	Return:
 *		void
 *	Description:
 *		This methods:
 *			+ increments the frequency parameter/duty parameter of this object (DACunit)
 *			+ sets a new value in the OCR1A according to the freqTAB
 *		the setting are made according to the MyLCD.blink_state variable (described in MyLCD class)
 *		The freqTAB has in the current implementation only 10 value for 100Hz, 200Hz, up to 900Hz.
 *		The freqTAB[1] has OCR1A value for 100Hz, freqTAB[2] has OCR1A value for 200Hz and so on
 */

{
	switch(MyLCD.blink_state)
	{
	case FREQU_CODE:
		//one +1 increment is never called in the current implementation
		this->incrementFreq(1);
		break;
	case FREQT_CODE:
		//one +10 increment is never called in the current implementation
		this->incrementFreq(10);
		break;
	case FREQH_CODE:
		//ONLY +100 increment is called in the current implementation!
		this->incrementFreq(100);
		this->setTimerRegister(freqTAB[this->freq/100]);
		break;
	case DUTYU_CODE:
		this->incrementDuty(1);
		break;
	case DUTYT_CODE:
		this->incrementDuty(10);
		break;
	default:
		break;
	}
}

//decrements
void DACunit::decrementFreq(uint8_t dec)
/*
 *	Name: decrementFreq
 *	Parameters:	uint8_t dec decrement to freq parameter
 *	Return:
 *		void
 *	Description:
 *		This methods decrements the freq parameter with some limitations
 *		Under no circumstances the freq-dec can become a negative number
 */
{
	if((int16_t)(this->freq-dec)>0)
		this->freq-=dec;
}

void DACunit::decrementDuty(uint8_t dec)
/*
 *	Name: decrementDuty
 *	Parameters:	uint8_t dec decrement to duty parameter
 *	Return:
 *		void
 *	Description:
 *		This methods decrements the duty parameter with some limitations
 *		Under no circumstances the duty-dec can become a negative number
 */
{
	if((int8_t)(this->duty-dec)>0)
		this->duty-=dec;
}

void DACunit::decrementParameter(LCDDisplay MyLCD)
/*
 *	Name: decrementParameter
 *	Parameters:
 *		LCDDisplay MyLCD object
 *		The DACunit class is friend of LCDDisplay and DACunit will be able to access all private and public members
 *	Return:
 *		void
 *	Description:
 *		This methods:
 *			+ decrements the frequency parameter/duty parameter of this object (DACunit)
 *			+ sets a new value in the OCR1A according to the freqTAB
 *		the setting are made according to the MyLCD.blink_state variable (described in MyLCD class)
 *		The freqTAB has in the current implementation only 10 value for 100Hz, 200Hz, up to 900Hz.
 *		The freqTAB[1] has OCR1A value for 100Hz, freqTAB[2] has OCR1A value for 200Hz and so on
 */
{
	switch(MyLCD.blink_state)
	{
	case FREQU_CODE:
		//one +1 increment is never called in the current implementation
		this->decrementFreq(1);
		break;
	case FREQT_CODE:
		//one +10 increment is never called in the current implementation
		this->decrementFreq(10);
		break;
	case FREQH_CODE:
		//ONLY +100 increment is called in the current implementation!
		this->decrementFreq(100);
		this->setTimerRegister(freqTAB[this->freq/100]);
		break;
	case DUTYU_CODE:
		this->decrementDuty(1);
		break;
	case DUTYT_CODE:
		this->decrementDuty(10);
		break;
	default:
		break;
	}
}

