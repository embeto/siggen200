# sigGen 200 - Low-cost custom-made Signal Generator  #
* https://embeto.com/siggen-200/
* Up to 1kHz output signal frequency.
* One double rail external power supply needed (+/-12V 0.5A) required
* +/-10V with 20VPP output signal.
* Fine and coarse tuning system for amplitude level.
* offset control from -9V to 9V
* waveforms included: sine, square (variable duty cycle), triangle, sawtooth and random noise

## Repository Structure ##
* 3Ddesign 
	* components - STEP models for different components
	* tapeouts - system enclosure 3D designs exported as STL
* documents 
	* datasheets  
	* schematics - each tapeout version is listed here in a directory containing the corresponding BOM and PDF schematic 
	* resources

* pcbDesign 
	* com_control_PCB
		* schematics - Working directory for com_control_PCB circuit board
		* tapeouts (see *circuit Board design* section for more)
	* main_PCB
		* schematics - Working directory for main_PCB
		* tapeouts (see *circuit Board design* section for more) - every PCB tapeout is listed here in a directory containing the exact schematic and gerber files

* sources 
	* shell - shell scripts such as one used to fuse MCU fuse bits
	* sigGen200_new - new implmentation to resolve frequency issue
	* sigGen200_RLD - cpp code for MCU used in current implementation

## Circuit Board design ##

* com_control_PCB
	* v1.2 - (factory produced) - Project version without 3D models for components
	* v1.3 - (dropped) - same electrical tracks as v1.2 but 3D component models added

* main_PCB
	* v1.2 - (factory produced) - Project version without 3D models for components, issues with voltage inverter from Linear Technologies and wrong crystal used (32kHz instead of 16Mhz)
	* v1.3 - (dropped) - almost same electrical tracks as v1.2 (sightly different routing at MCU pins) but 3D models added for all components, same electrical and design issues as v1.2
	* v1.4 - (dropped) - concept schematic with dual rail supply needed (IC convertor from +12V to -12V was removed due to high noise introduced in circuit). Crystal issues still unresolved. For this version of schematic only PDF is available and none of the orcad/allegro files.

## 3D design ##
* view/download the 3D enclosure design in Web Browser [here](https://a360.co/35NhCIv)
